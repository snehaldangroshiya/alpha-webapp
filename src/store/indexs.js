import Vue from 'vue'
import Vuex from 'vuex'
import route from "../router"

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    user: null,
    error: null,
    loading: false
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    }
  },
  actions: {
    userSignUp ({commit}, payload) {
      commit('setLoading', true);
      console.log("singup call");
    },
    userSignIn ({commit}, payload) {
      commit('setLoading', true)
      console.log("signin")
      route.push('/#/webapp');
    }
  }
});
