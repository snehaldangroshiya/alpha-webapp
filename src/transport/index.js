import axios from 'axios'

export const Tr = axios.create({
  baseURL: `http://localhost:8000/api/v1/`,
  withCredentials: true,
  headers: {
    'Content-type': 'application/x-www-form-urlencoded',
  }
})
