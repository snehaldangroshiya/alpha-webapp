import Vue from 'vue'
import Router from 'vue-router'
import WebApp from '@/components/webapp'
import ContentPanel from '@/components/ContentPanel'
import Login from '@/components/Login'
import Spiner from '@/components/LoadingMask/Spiner'
import store from '../store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isLoggedIn) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isLoggedIn) {
    next()
    return
  }
  next('/')
}

export default new Router({
  routes:[{
    path: '/',
    name: 'login',
    component: Login
  }, {
    path: '/spiner',
    name: 'Spiner',
    component: Spiner
  }, {
    path: '/webapp',
    name: 'WebApp',
    component: WebApp,
    beforeEnter : ifAuthenticated,
    children: [{
      path: '/',
      name: 'ContentPanel',
      component: ContentPanel    
    }]
  }],
   mode:'history',
   base: __dirname
})
