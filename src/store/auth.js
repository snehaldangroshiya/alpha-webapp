const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGOUT = "LOGOUT";
import {Tr} from '../transport'

export default {
    state: {
        isLoggedIn: !!localStorage.getItem("token")
    },
    mutations: {
        [LOGIN](state) {
            state.pending = true;
        },
        [LOGIN_SUCCESS](state) {
            state.isLoggedIn = true;
            state.pending = false;
        },
        [LOGOUT](state) {
            state.isLoggedIn = false;
        }
    },
    actions: {
        login({state, commit, rootState}, payload) {
            var me = this;
            commit(LOGIN);
            Tr.post("user/login", payload)
            .then(res => res.data)
            .then(data => {
                localStorage.setItem("token", data.token);
                this.commit(LOGIN_SUCCESS)
            })           
        },
        logout({commit}) {
            localStorage.removeItem("token");
            commit(LOGOUT);
        }
    },
    getters: {
        isLoggedIn: state => {
            return state.isLoggedIn;
        }
    }
}